<?php

function ttn_api_diskak_init( $server ) {
	global $ttn_api_diskak;

	$ttn_api_diskak = new TTN_API_Diskak( $server );
	add_filter( 'json_endpoints', array( $ttn_api_diskak, 'register_routes' ) );
}
add_action( 'wp_json_server_before_serve', 'ttn_api_diskak_init' );


class TTN_API_Diskak extends WP_JSON_CustomPostType {

  protected $base = '/diskak';
  protected $type = 'diskak';

  public function register_routes( $routes ) {
		$routes['/diskak'] = array(
			array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
		);

		$routes['/diskak/count'] = array(
			array( array( $this, 'get_count'), WP_JSON_Server::READABLE ),
		);

		$routes['/diskak/(?P<id>\d+)'] = array(
			array( array( $this, 'get_post'), WP_JSON_Server::READABLE ),
		);

		// Add more custom routes here

		return $routes;
	}

	function get_count() {
  		$count = wp_count_posts( 'diskak', 'readable' );
			$result = new stdClass();
			$result->count = intval($count->publish);

  		return $result;
  }


  function get_posts( $filter = array(), $context = 'ttn', $type = null, $page = 1, $size = 30  ) {

		$page = intval($page);
		$size = intval($size);
		if($page<=0) {
			$page=1;
		}
		if($size<=0) {
			$size=1;
		}

    $posts = get_posts(array(
      'post_type'		=> 'diskak',
      'posts_per_page'	=> $size,
			'offset'	=> ($page-1)*$size
    ));

    $response = new WP_JSON_Response();
    if ( count($posts)<1 ) {
      $response->set_data( array() );
      return $response;
    }
    $response = array();
    foreach ( $posts as $post ) {
      $post = get_object_vars( $post );
      $post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);
      $response[] = $post_data;
    }
    return $response;
  }

  function get_post( $id, $context = 'ttn' ) {
  		$post = get_post(intval($id));
  		$response = new WP_JSON_Response();
  		//$response->query_navigation_headers( $post_query );
  		if ( !isset($post) ) {
  			$response->set_data( new stdClass() );
  			return $response;
  		}
			$post = get_object_vars( $post );
			$post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);

      $response->set_data($post_data);
  		return $response;
  }

}

add_filter( 'json_prepare_post', function ($data, $post, $context) {
	global $APIUtils;

  if($post['post_type']==='diskak'){
			$zigiluaData = get_field('zigilua', $post['ID'], true)[0];
			if($zigiluaData){
				$zigilua = new stdClass();
				$zigilua->ID = $zigiluaData->ID;
				$zigilua->title = $zigiluaData->post_title;
				$zigilua->slug = $zigiluaData->post_name;
				$zigilua->link = get_permalink($zigiluaData->ID);
				$zigilua->image = $APIUtils->getPostImage($zigilua->ID);

				$data['zigilua']  = $zigilua;
			}

			$taldeakData = get_field('taldeak', $post['ID'], true);
			if($taldeakData){
				$taldeak = [];
				foreach ($taldeakData as $key => $taldea) {
					$t = $APIUtils->unsetData($taldea);
					$t->meta = $APIUtils->getAPIMetaLinks($taldea->ID, 'taldeak');
					$t->image = $APIUtils->getPostImage($taldea->ID);

					$taldeak[] = $t;
				}
				$data['taldeak'] = $taldeak;
			}

			$elkarrizketa = get_posts(array(
        'meta_query' => array(
            array(
                'key' => 'diskak',
                'value' => '"' . $post['ID'] . '"',
                'compare' => 'LIKE'
            )
        )
			));
			if(isset($elkarrizketa[0])) {
				$data['elkarrizketa'] = $APIUtils->unsetData($elkarrizketa[0]);
				$data['elkarrizketa']->enclosure = $APIUtils->getEnclosure($elkarrizketa[0]->ID);
			}
  }
  return $data;
}, 11, 3 );
