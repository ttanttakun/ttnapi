<?php

class TTN_API_Utils {

    private static $_instance = null;
    private static $APIUrl;
    private static $APITimeZone;

    private function __construct() {
      self::$APIUrl = get_site_url()."/wp-json/";
      self::$APITimeZone = new DateTimeZone('Europe/Madrid');
    }
    private function __clone() {}
    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }


    function getIrratsaioaFromCategories($post_categories) {
      $slug = 'irratsaioak';
      $result = false;
      if($post_categories){
        foreach ($post_categories as $key => $category) {
          if(is_array($category)){
            if($category['parent']['slug']===$slug) {
              $result = $category;
              break;
            }
          }
        }
      }

      $cat = get_term_by( 'slug', $result['slug'], 'category');

      $irratsaioa = get_posts(array(
        'post_type' => 'irratsaioa',
        'meta_query' => array(
            array(
                'key' => 'irratsaio_kategoria',
                'value' => '"' . $cat->term_id . '"',
                'compare' => 'LIKE'
            )
        )
			));
			if(isset($irratsaioa[0])) {
        $irratsaioa = self::$_instance->unsetData($irratsaioa[0]);
        $irratsaioa->image = self::$_instance->getPostImage($irratsaioa->ID);
      } else {
        $irratsaioa = null;
      }

      return $irratsaioa;
    }

    function getEnclosure($post_id){
      $enclosure = explode("\n",get_post_meta( $post_id, 'enclosure', true ));
      $result = new stdClass();
      $result->src = trim($enclosure[0]);
      $result->size = trim($enclosure[1]);
      $result->type = trim($enclosure[2]);

      return $result;
    }


    function unsetData($data){
      if(is_array($data)){
        unset($data['status']);
        unset($data['parent']);
        unset($data['format']);
        unset($data['menu_order']);
        unset($data['comment_status']);
        unset($data['ping_status']);
        unset($data['sticky']);
        unset($data['guid']);
        unset($data['featured_image']);
        unset($data['author']);
        unset($data['date_gmt']);
        unset($data['date_tz']);
        unset($data['modified_tz']);
        unset($data['modified_gmt']);
        $data['date'] = self::$_instance->prepareDate($data['date']);
      } else {
        unset($data->post_status);
        unset($data->post_parent);
        unset($data->post_format);
        unset($data->post_password);
        unset($data->menu_order);
        unset($data->post_modified);
        unset($data->post_modified_gmt);
        unset($data->post_date_gmt);
        unset($data->comment_status);
        unset($data->comment_count);
        unset($data->ping_status);
        unset($data->to_ping);
        unset($data->pinged);
        unset($data->post_mime_type);
        unset($data->filter);
        unset($data->post_author);
        unset($data->post_content_filtered);
        unset($data->guid);

        $data->type = $data->post_type;
        unset($data->post_type);

        $data->title = $data->post_title;
        unset($data->post_title);

        $data->name = $data->post_name;
        $data->slug = $data->post_name;
        unset($data->post_name);

        $data->content = $data->post_content;
        unset($data->post_content);

        $data->date = self::$_instance->prepareDate($data->post_date);
        unset($data->post_date);

        $data->excerpt = $data->post_excerpt;
        unset($data->post_excerpt);
      }

      return $data;
    }


    function getPostImage($ID){
      if(get_post_thumbnail_id( $ID )){
        $image = new stdClass();
        $image->thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), "thumbnail")[0];
        $image->medium = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), "medium")[0];
        $image->large = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), "large")[0];
        $image->full = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), "full")[0];
      } else {
        $image = null;
      }

      return $image;
    }


    function getAPIMetaLinks($id, $service){
      $meta = new stdClass();
      $links = new stdClass();
      $links->self = self::$APIUrl.$service.'/'.$id;
      $links->collection = self::$APIUrl.$service;
      $meta->links = $links;

      return $meta;
    }

    function prepareDate($date){
      $d = new DateTime($date, self::$APITimeZone);

      return $d->format(DateTime::ISO8601);
    }
}
