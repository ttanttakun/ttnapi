<?php

function ttn_api_aretoak_init( $server ) {
	global $ttn_api_aretoak;

	$ttn_api_aretoak = new TTN_API_Aretoak( $server );
	add_filter( 'json_endpoints', array( $ttn_api_aretoak, 'register_routes' ) );
}
add_action( 'wp_json_server_before_serve', 'ttn_api_aretoak_init' );


class TTN_API_Aretoak extends WP_JSON_CustomPostType {

  protected $base = '/aretoak';
  protected $type = 'aretoak';

  public function register_routes( $routes ) {
		$routes['/aretoak'] = array(
			array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
		);

		$routes['/aretoak/count'] = array(
			array( array( $this, 'get_count'), WP_JSON_Server::READABLE ),
		);


		$routes['/aretoak/(?P<id>\d+)'] = array(
			array( array( $this, 'get_post'), WP_JSON_Server::READABLE ),
		);

		// Add more custom routes here

		return $routes;
	}

	function get_count() {
  		$count = wp_count_posts( 'aretoak', 'readable' );
			$result = new stdClass();
			$result->count = intval($count->publish);

  		return $result;
  }


  function get_posts( $filter = array(), $context = 'ttn', $type = null, $page = 1, $size = 30  ) {

		$page = intval($page);
		$size = intval($size);
		if($page<=0) {
			$page=1;
		}
		if($size<=0) {
			$size=1;
		}

    $posts = get_posts(array(
      'post_type'		=> 'aretoak',
      'posts_per_page'	=> $size,
			'offset'	=> ($page-1)*$size
    ));

    $response = new WP_JSON_Response();
    if ( count($posts)<1 ) {
      $response->set_data( array() );
      return $response;
    }
    $response = array();
    foreach ( $posts as $post ) {
      $post = get_object_vars( $post );
      $post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);
      $response[] = $post_data;
    }
    return $response;
  }

  function get_post( $id, $context = 'ttn' ) {
  		$post = get_post(intval($id));
  		$response = new WP_JSON_Response();
  		//$response->query_navigation_headers( $post_query );
  		if ( !isset($post) ) {
  			$response->set_data( new stdClass() );
  			return $response;
  		}
			$post = get_object_vars( $post );
			$post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);

      $response->set_data($post_data);
  		return $response;
  }

}

add_filter( 'json_prepare_post', function ($data, $post, $context) {
	global $APIUtils;
	global $TTNMusika;
  if($post['post_type']==='aretoak'){
		$kontzertuakData = $TTNMusika->getUpcomingEvents('aretoak', $data['ID']);

		if($kontzertuakData){
			$kontzertuak = [];
			foreach ($kontzertuakData as $key => $value) {
				$kontzertua = $APIUtils->unsetData($value);
				$kontzertua->meta = $APIUtils->getAPIMetaLinks($value->ID, 'kontzertuak');
				$kontzertua->image = $APIUtils->getPostImage($value->ID);
				$eguna = get_field('data', $value->ID);
				$kontzertua->startDate = $APIUtils->prepareDate($eguna);

				$taldeakData = get_field('taldeak', $value->ID, true);
				if($taldeakData){
					$taldeak = [];
					foreach ($taldeakData as $key => $taldea) {
						$band = $APIUtils->unsetData($taldea);
						$band->meta = $APIUtils->getAPIMetaLinks($taldea->ID, 'taldeak');
						$band->image = $APIUtils->getPostImage($band->ID);
						$taldeak[] = $band;
					}
					$kontzertua->taldeak = $taldeak;
				}
				$kontzertuak[] = $kontzertua;
			}
			$data['kontzertuak'] = $kontzertuak;
		}
  }

	$helbidea = get_field('helbidea', $post['ID'], true);
	if($helbidea){
		$data['helbidea'] = $helbidea;
	}
	$hiria = get_field('hiria', $post['ID'], true);
	if($hiria){
		$data['hiria'] = $hiria;
	}
	$webgunea = get_field('webgunea', $post['ID'], true);
	if($webgunea){
		$data['webgunea'] = $webgunea;
	}

	$geokokapena = get_field('geokokapena', $post['ID'], true);
	if($geokokapena){
		$data['geokokapena'] = $geokokapena;
	}

  return $data;
}, 11, 3 );
