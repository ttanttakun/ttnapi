<?php

function ttn_api_irratsaioak_init( $server ) {
	global $ttn_api_irratsaioak;

	$ttn_api_irratsaioak = new TTN_API_Irratsaioak( $server );
	add_filter( 'json_endpoints', array( $ttn_api_irratsaioak, 'register_routes' ) );
}
add_action( 'wp_json_server_before_serve', 'ttn_api_irratsaioak_init' );


class TTN_API_Irratsaioak extends WP_JSON_CustomPostType {

  protected $base = '/irratsaioak';
  protected $type = 'irratsaioa';

  public function register_routes( $routes ) {
		$routes['/irratsaioak'] = array(
			array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
		);

		$routes['/irratsaioak/count'] = array(
			array( array( $this, 'get_count'), WP_JSON_Server::READABLE ),
		);

		$routes['/irratsaioak/(?P<id>\d+)'] = array(
			array( array( $this, 'get_post'), WP_JSON_Server::READABLE ),
		);

		// Add more custom routes here

		return $routes;
	}

	function get_count($egoera = 'on') {
		$eg = 1;
		if($egoera!=='on') {
			$eg=0;
		}

		$args = array(
			'post_type'       => 'irratsaioa',
			'posts_per_page'	=>-1,
			'meta_key'        => 'on_air',
			'meta_value'      => $eg,
			'exclude' 				=> 15635
		);

    $posts = query_posts( $args );
		$result = new stdClass();
    $result->count = count($posts);

    return $result;
  }


  function get_posts( $filter = array(), $context = 'ttn', $type = null, $page = 1, $size = 30, $egoera = 'on'  ) {

    $eg = 1;
    if($egoera!=='on') {
      $eg=0;
    }

		$page = intval($page);
		$size = intval($size);
		if($page<=0) {
			$page=1;
		}
		if($size<=0) {
			$size=1;
		}

    $posts = get_posts(array(
      'post_type'		=> 'irratsaioa',
      'posts_per_page'	=> $size,
			'offset' => ($page-1)*$size,
      'meta_key'		=> 'on_air',
      'meta_value'		=> $eg,
			'exclude'				=> 15635
    ));

    $response = new WP_JSON_Response();
    if ( count($posts)<1 ) {
      $response->set_data( array() );
      return $response;
    }
    $response = array();
    foreach ( $posts as $post ) {
      $post = get_object_vars( $post );
      $post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);
      $response[] = $post_data;
    }
    return $response;
  }

  function get_post( $id, $context = 'ttn' ) {
  		$post = get_post(intval($id));
  		$response = new WP_JSON_Response();
  		//$response->query_navigation_headers( $post_query );
  		if ( !isset($post) ) {
  			$response->set_data( new stdClass() );
  			return $response;
  		}
			$post = get_object_vars( $post );
			$post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);

      $response->set_data($post_data);
  		return $response;
  }

}

add_filter( 'json_prepare_post', function ($data, $post, $context) {

  if($post['post_type']==='irratsaioa'){
		$categoryData = get_field('irratsaio_kategoria', $post['ID'], true);
		$category = new stdClass();
		$category->ID = $categoryData->term_id;
		$category->name = $categoryData->name;
		$category->slug = $categoryData->slug;

		$categoryData[0]->ID = $categoryData[0]->term_id;

		$data['terms']['category']  = $categoryData;
  }
  return $data;
}, 11, 3 );
