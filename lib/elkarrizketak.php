<?php

function ttn_api_elkarrizketak_init( $server ) {
	global $ttn_api_elkarrizketak;

	$ttn_api_elkarrizketak = new TTN_API_Elkarrizketak( $server );
	add_filter( 'json_endpoints', array( $ttn_api_elkarrizketak, 'register_routes' ) );
}
add_action( 'wp_json_server_before_serve', 'ttn_api_elkarrizketak_init' );


class TTN_API_Elkarrizketak extends WP_JSON_CustomPostType {

  protected $base = '/elkarrizketak';
  protected $type = 'post';

  public function register_routes( $routes ) {
		$routes['/elkarrizketak'] = array(
			array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
		);

		$routes['/elkarrizketak/count'] = array(
			array( array( $this, 'get_count'), WP_JSON_Server::READABLE ),
		);

		$routes['/elkarrizketak/(?P<id>\d+)'] = array(
			array( array( $this, 'get_post'), WP_JSON_Server::READABLE ),
		);

		// Add more custom routes here

		return $routes;
	}


	function get_count() {
			$cat = get_term_by( 'term_id', 9, 'category');
			$result = new stdClass();
			$result->count = intval($cat->count);

			return $result;
	}



  function get_posts( $filter = array(), $context = 'ttn', $type = null, $page = 1, $size = 30  ) {
		$page = intval($page);
		$size = intval($size);
    if($page<=0) {
      $page=1;
    }
		if($size<=0) {
      $size=1;
    }
		$args = array(
			'posts_per_page' => $size ,
			'cat' => 9,
			'offset' => ($page-1)*$size
		);

    $posts = get_posts($args);

    $response = new WP_JSON_Response();
    if ( count($posts)<1 ) {
      $response->set_data( array() );
      return $response;
    }
    $response = array();
    foreach ( $posts as $post ) {
      $post = get_object_vars( $post );
      $post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);
      $response[] = $post_data;
    }
    return $response;
  }

  function get_post( $id, $context = 'ttn' ) {
  		$post = get_post(intval($id));
  		$response = new WP_JSON_Response();
  		//$response->query_navigation_headers( $post_query );
  		if ( !isset($post) ) {
  			$response->set_data( new stdClass() );
  			return $response;
  		}
			$post = get_object_vars( $post );
			$post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);

      $response->set_data($post_data);
  		return $response;
  }

}

add_filter( 'json_prepare_post', function ($data, $post, $context) {
	global $APIUtils;
	if(has_term( 'elkarrizketak', 'category', $post['ID'] )){
		$taldeakData = get_field('taldeak', $data['ID'], true);
		if($taldeakData){
			$taldeak = [];
			foreach ($taldeakData as $key => $taldea) {
				$t = $APIUtils->unsetData($taldea);
				$t->meta = $APIUtils->getAPIMetaLinks($taldea->ID, 'taldeak');
				$t->image = $APIUtils->getPostImage($taldea->ID);

				$taldeak[] = $t;
			}
			$data['taldeak'] = $taldeak;
		} else {
			$data['taldeak'] = null;
		}

		$diskakData = get_field('diskak', $data['ID'], true);
		if($diskakData){
			$diskak = [];
			foreach ($diskakData as $key => $diska) {
				$t = $APIUtils->unsetData($diska);
				$t->meta = $APIUtils->getAPIMetaLinks($diska->ID, 'diskak');
				$t->image = $APIUtils->getPostImage($diska->ID);

				$diskak[] = $t;
			}
			$data['diskak'] = $diskak;
		} else {
			$data['diskak'] = null;
		}
	}

  return $data;
}, 11, 3 );
