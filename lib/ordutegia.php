<?php
function ttn_ordutegia_api_init() {
	global $ttn_ordutegia_api;

	$ttn_ordutegia_api = new TTN_Ordutegia();
	add_filter( 'json_endpoints', array( $ttn_ordutegia_api, 'register_routes' ) );
}
add_action( 'wp_json_server_before_serve', 'ttn_ordutegia_api_init' );

class TTN_Ordutegia {

	public function register_routes( $routes ) {
		$routes['/ordutegia'] = array(
			array(
        array( $this, 'get_ordutegia_legacy'), WP_JSON_Server::READABLE
      )
		);
		$routes['/ordutegia/(?P<id>\d+)'] = array(
			array(
				array( $this, 'get_ordutegia'), WP_JSON_Server::READABLE
			),
			array(
				array( $this, 'set_ordutegia'), WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON
			)
		);

		return $routes;
	}

  function get_ordutegia_legacy() {
    $ordutegia = get_option('ttn-ordutegia-json-1',[]);
    $response = new WP_JSON_Response($ordutegia);

    return $response;
  }

	function get_ordutegia( $id ) {
		$ordutegia = get_option('ttn-ordutegia-json-'.$id,[]);
    $response = new WP_JSON_Response($ordutegia);

    return $response;
	}

  function set_ordutegia( $id, $data) {
		$token = get_option("X_TTN_AUTH");
		if($_headers["X_TTN_AUTH"] === $token) {
			update_option('ttn-ordutegia-json-'.$id, $data);
		}

    return "ok";
  }
}
