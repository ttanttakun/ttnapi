<?php

function ttn_api_kontzertuak_init( $server ) {
	global $ttn_api_kontzertuak;

	$ttn_api_kontzertuak = new TTN_API_Kontzertuak( $server );
	add_filter( 'json_endpoints', array( $ttn_api_kontzertuak, 'register_routes' ) );
}
add_action( 'wp_json_server_before_serve', 'ttn_api_kontzertuak_init' );


class TTN_API_Kontzertuak extends WP_JSON_CustomPostType {

  protected $base = '/kontzertuak';
  protected $type = 'kontzertuak';

  public function register_routes( $routes ) {
		$routes['/kontzertuak'] = array(
			array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
		);

		$routes['/kontzertuak/count'] = array(
			array( array( $this, 'get_count'), WP_JSON_Server::READABLE ),
		);

		$routes['/kontzertuak/(?P<id>\d+)'] = array(
			array( array( $this, 'get_post'), WP_JSON_Server::READABLE ),
		);

		// Add more custom routes here

		return $routes;
	}

	function get_count() {
		// TTNMusika-n egon beharko zen...
		$args = array(
			'order'=>'ASC',
			'orderby' => 'meta_value_num',
			'meta_key'=>'data',
			'posts_per_page'	=>-1,
			'post_type'		=> 'kontzertuak',
			'meta_query' => array(
				array(
				'key' => 'data',
				'value' => time(),
				'compare' => '>',
				'type' => 'NUMERIC'
				),
			)
		);

		$kontzertuak = new WP_Query( $args );
		$result = new stdClass();
		$result->count = intval($kontzertuak->post_count);

		return $result;
  }


  function get_posts( $filter = array(), $context = 'ttn', $type = null, $page = 1, $size = 30  ) {
		global $TTNMusika;

		$page = intval($page);
		$size = intval($size);
		if($page<=0) {
			$page=1;
		}
		if($size<=0) {
			$size=1;
		}

    $posts = $TTNMusika->getUpcomingEvents('all', null, $page, $size);

    $response = new WP_JSON_Response();
    if ( count($posts)<1 ) {
      $response->set_data( array() );
      return $response;
    }
    $response = array();
    foreach ( $posts as $post ) {
      $post = get_object_vars( $post );
      $post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);
      $response[] = $post_data;
    }
    return $response;
  }

  function get_post( $id, $context = 'ttn' ) {
  		$post = get_post(intval($id));
  		$response = new WP_JSON_Response();
  		//$response->query_navigation_headers( $post_query );
  		if ( !isset($post) ) {
  			$response->set_data( new stdClass() );
  			return $response;
  		}
			$post = get_object_vars( $post );
			$post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);

      $response->set_data($post_data);
  		return $response;
  }

}

add_filter( 'json_prepare_post', function ($data, $post, $context) {
	global $APIUtils;

  if($post['post_type']==='kontzertuak'){

			$eguna = get_field('data', $post['ID']);
			$data['startDate'] = $APIUtils->prepareDate($eguna);

			$taldeakData = get_field('taldeak', $post['ID'], true);
			if($taldeakData){
				$taldeak = [];
				foreach ($taldeakData as $key => $taldea) {
					$t = $APIUtils->unsetData($taldea);
					$t->meta = $APIUtils->getAPIMetaLinks($taldea->ID, 'taldeak');
					$t->image = $APIUtils->getPostImage($taldea->ID);

					$taldeak[] = $t;
				}
				$data['taldeak'] = $taldeak;
			}

			$aretoakData = get_field('aretoak', $post['ID'], true);
			if($aretoakData){
				$aretoak = [];
				foreach ($aretoakData as $key => $aretoa) {
					$t = $APIUtils->unsetData($aretoa);
					$t->meta = $APIUtils->getAPIMetaLinks($aretoa->ID, 'aretoak');
					$t->image = $APIUtils->getPostImage($aretoa->ID);

					$aretoak[] = $t;
				}
				$data['aretoak'] = $aretoak;
			}

			$elkarrizketa = get_posts(array(
        'meta_query' => array(
            array(
                'key' => 'kontzertuak',
                'value' => '"' . $post['ID'] . '"',
                'compare' => 'LIKE'
            )
        )
			));
			if(isset($elkarrizketa[0])) {
				$data['elkarrizketa'] = $APIUtils->unsetData($elkarrizketa[0]);
				$data['elkarrizketa']->enclosure = $APIUtils->getEnclosure($elkarrizketa[0]->ID);
				$data['elkarrizketa']->meta = $APIUtils->getAPIMetaLinks($elkarrizketa[0]->ID, 'elkarrizketak');
			}

			$sarreraData = get_field('sarrera', $post['ID']);
			if($sarreraData){
				$data['sarrera'] = $sarreraData;
			}

			$webgunea = get_field('webgunea', $post['ID'], true);
			if($webgunea){
				$data['webgunea'] = $webgunea;
			}
  }
  return $data;
}, 11, 3 );
