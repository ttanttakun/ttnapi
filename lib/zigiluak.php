<?php

function ttn_api_zigiluak_init( $server ) {
	global $ttn_api_zigiluak;

	$ttn_api_zigiluak = new TTN_API_Zigiluak( $server );
	add_filter( 'json_endpoints', array( $ttn_api_zigiluak, 'register_routes' ) );
}
add_action( 'wp_json_server_before_serve', 'ttn_api_zigiluak_init' );


class TTN_API_Zigiluak extends WP_JSON_CustomPostType {

  protected $base = '/zigiluak';
  protected $type = 'zigiluak';

  public function register_routes( $routes ) {
		$routes['/zigiluak'] = array(
			array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
		);

		$routes['/zigiluak/count'] = array(
			array( array( $this, 'get_count'), WP_JSON_Server::READABLE ),
		);

		$routes['/zigiluak/(?P<id>\d+)'] = array(
			array( array( $this, 'get_post'), WP_JSON_Server::READABLE ),
		);

		// Add more custom routes here

		return $routes;
	}

	function get_count() {
  		$count = wp_count_posts( 'zigiluak', 'readable' );
			$result = new stdClass();
			$result->count = intval($count->publish);

  		return $result;
  }

  function get_posts( $filter = array(), $context = 'ttn', $type = null, $page = 1, $size = 30  ) {

		$page = intval($page);
		$size = intval($size);
		if($page<=0) {
			$page=1;
		}
		if($size<=0) {
			$size=1;
		}

    $posts = get_posts(array(
      'post_type'		=> 'zigiluak',
      'posts_per_page'	=> $size,
			'offset'	=> ($page-1)*$size
    ));

    $response = new WP_JSON_Response();
    if ( count($posts)<1 ) {
      $response->set_data( array() );
      return $response;
    }
    $response = array();
    foreach ( $posts as $post ) {
      $post = get_object_vars( $post );
      $post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);
      $response[] = $post_data;
    }
    return $response;
  }

  function get_post( $id, $context = 'ttn' ) {
  		$post = get_post(intval($id));
  		$response = new WP_JSON_Response();
  		//$response->query_navigation_headers( $post_query );
  		if ( !isset($post) ) {
  			$response->set_data( new stdClass() );
  			return $response;
  		}
			$post = get_object_vars( $post );
			$post_data = $this->prepare_post( $post, $context );
			unset($post_data['meta']['links']['author']);
			unset($post_data['meta']['links']['replies']);
			unset($post_data['meta']['links']['version-history']);

      $response->set_data($post_data);
  		return $response;
  }

}

add_filter( 'json_prepare_post', function ($data, $post, $context) {
	global $APIUtils;
  if($post['post_type']==='zigiluak'){
		$diskak = get_posts(array(
        'post_type' => 'diskak',
        'meta_query' => array(
            array(
                'key' => 'zigilua',
                'value' => '"' . $data['ID'] . '"',
                'compare' => 'LIKE'
            )
        )
    ));

		$data['diskak'] = [];
		foreach ($diskak as $key => $value) {
			$tmpDiska = $APIUtils->unsetData($value);
			$tmpDiska->image = $APIUtils->getPostImage($value->ID);
			$tmpDiska->meta = $APIUtils->getAPIMetaLinks($value->ID, 'diskak');
			$data['diskak'][] = $tmpDiska;
		}
  }
  return $data;
}, 11, 3 );
