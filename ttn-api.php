<?php
/**
 * Plugin Name: TTN API
 * Description: TTN API Sistema
 * Version: 0.1
 * Author: Jimakker
 * Author URI: http://twitter.com/jimakker
 */
require_once('lib/utils.php');
$APIUtils = TTN_API_Utils::getInstance();

require_once('lib/irratsaioak.php');
require_once('lib/grabazioak.php');
require_once('lib/taldeak.php');
require_once('lib/diskak.php');
require_once('lib/zigiluak.php');
require_once('lib/aretoak.php');
require_once('lib/elkarrizketak.php');
require_once('lib/kontzertuak.php');
require_once('lib/berriak.php');
require_once('lib/ordutegia.php');
// require_once('lib/estiloak.php');




add_filter( 'json_prepare_post', function ($data, $post, $context) {
  global $APIUtils;
  $custom_api = ['irratsaioa', 'diskak', 'zigiluak', 'taldeak', 'aretoak', 'kontzertuak'];
  if(
    in_array($post['post_type'], $custom_api) ||
    has_term( 'audioak', 'category', $post['ID'] ) ||
    has_term( 'berriak', 'category', $post['ID'] )
  ){

    $data = $APIUtils->unsetData($data);

    $data['image'] = $APIUtils->getPostImage($data['ID']);

  }
  return $data;
}, 12, 3 );
